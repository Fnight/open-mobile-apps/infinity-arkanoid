﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Firebase.Analytics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ScoreModel : MonoBehaviour
{
    public bool IsLose = false;
    public float AcrossBonus = 0;
    public float TimeAcrossBonus = 3;
    public float SpeedBonusTime = 3;
    public float SpeedBonus = 0;
    public float SpikeBonus = 0;
    public float SpeedBonusCooficient = 2;
    public Action OnLose;

    [HideInInspector] public List<Int32> Balls = new List<Int32>();

    public PostProcessVolume ProcessVolume;
    public float ShakeTime = 0.5f;
    public float ShakeAmplitude = 1;
    public bool IsPause = false;
    public GameObject PausePanel;
    public GameObject PauseButtons;
    public GameObject PauseButton;
    public GameObject PlayButton;
    public GameObject AcrossBonusIndicator;
    public Text AcrossBonusTimeText;
    public GameObject SpeedBonusIndicator;
    public Text SpeedBonusTimeText;
    public Text NewScoreText;
    public Text MaxScoreText;
    public Text NewComboText;
    public Text MaxComboText;
    public Text ScoreText;
    public Text PauseTitleText;
    public Text ComboText;
    public GameObject CoinObject;
    public float TimeToHideCoin = 2;
    public GameObject[] WallsParents;
    public GameObject[] WallsChildren;
    public GameObject[] PaddleParts;
    public Text SpikeBonusText;
    public GameObject SpikeBonusButton;
    public float SpikeBonusTime = 1f;
    public GameObject SpikeBonusIndicator;
    public Text SpikeBonusTimeText;
    public Transform PositionToLaunchBall;
    public GameObject BallPrefab;
    public GameObject[] Hearts;

    public CinemachineVirtualCamera Camera;
    private CinemachineBasicMultiChannelPerlin channelPerlin;

    public Skin CurrentSkin { get; private set; }
    private int score = 0;
    private int combo = 0;
    private int maxCombo = 0;
    private int lifes = 0;

    private float shakeTimeElapsed = 0;
    public SaveDto Save;

    // Start is called before the first frame update
    void Start()
    {
        Save = SaveSystem.LoadSave();
        lifes = Save.Lifes;
        CurrentSkin = Resources.Load<Skin>(Save.CurrentSkin);
        Application.targetFrameRate = 60;
        UpdateGraphics();
        UpdateHearts();
        channelPerlin = Camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        PausePanel.GetComponent<Animator>().enabled = false;
        AddSpeedBonus();
        ColorWalls();
        ColorPaddle();
        UpdateSpikeButton();
    }

    private void UpdateSpikeButton()
    {
        if (Save.SpikeBonus == 0)
        {
            SpikeBonusButton.SetActive(false);
        }
        else
        {
            SpikeBonusText.text = Save.SpikeBonus.ToString();
        }
    }

    public void AddSpikeBonus()
    {
        SpikeBonus += SpikeBonusTime;
        Save.SpikeBonus--;
        SaveSystem.SaveData(Save);
        UpdateSpikeButton();
    }

    private void ColorPaddle()
    {
        foreach (var part in PaddleParts)
        {
            part.GetComponent<MeshRenderer>().material = CurrentSkin.Paddle;
        }
    }

    private void ColorWalls()
    {
        foreach (var wall in WallsChildren)
        {
            foreach (var renderer in wall.GetComponentsInChildren<MeshRenderer>())
            {
                if (renderer.gameObject != wall.gameObject)
                {
                    renderer.material = CurrentSkin.Block;
                }
            }
        }

        foreach (var wall in WallsParents)
        {
            wall.GetComponent<MeshRenderer>().material = CurrentSkin.Block;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (shakeTimeElapsed > 0)
        {
            channelPerlin.m_AmplitudeGain = ShakeAmplitude;
            shakeTimeElapsed -= Time.deltaTime;
        }
        else
        {
            channelPerlin.m_AmplitudeGain = 0;
        }

        if (AcrossBonus > 0)
        {
            AcrossBonus -= Time.deltaTime;
            AcrossBonusTimeText.text = PrepareBonusTimeText(AcrossBonus);
        }

        if (SpeedBonus > 0)
        {
            SpeedBonus -= Time.deltaTime;
            SpeedBonusTimeText.text = PrepareBonusTimeText(SpeedBonus);
        }

        if (SpikeBonus > 0)
        {
            SpikeBonus -= Time.deltaTime;
            SpikeBonusTimeText.text = PrepareBonusTimeText(SpikeBonus);
        }

        AcrossBonusIndicator.SetActive(AcrossBonus > 0);
        SpeedBonusIndicator.SetActive(SpeedBonus > 0);
        SpikeBonusIndicator.SetActive(SpikeBonus > 0);
    }

    private string PrepareBonusTimeText(float time)
    {
        var rounder = Math.Round(time, 1);
        return rounder == 0 ? "" : $"{rounder}";
    }

    private void UpdateGraphics()
    {
        Bloom bloom = null;
        ProcessVolume.profile.TryGetSettings(out bloom);
        if (bloom != null)
        {
            bloom.enabled.value = !Save.IsFast;
        }
    }

    public void BreakBlock()
    {
        score++;
        ScoreText.text = $"{score}";
        combo++;
        if (combo > 1)
        {
            ComboText.text = $"x{combo}";
        }

        if (maxCombo < combo)
        {
            maxCombo = combo;
        }
    }

    public void AddAcrossBonus()
    {
        AcrossBonus += TimeAcrossBonus;
    }

    public void AddSpeedBonus()
    {
        SpeedBonus += SpeedBonusTime;
    }

    public void CollidePaddle()
    {
        combo = 0;
        ComboText.text = "";
    }

    public void Restart()
    {
        Time.timeScale = 1;
        GameObject.Find("Level Loader").GetComponent<LevelLoader>().LoadScene(SceneManager.GetActiveScene().name);
    }

    public void CollectCoin()
    {
        Save.Money++;
        CoinObject.SetActive(true);
        CoinObject.GetComponent<Text>().text = $"{Save.Money}";
        StartCoroutine(HideCoin(TimeToHideCoin));
    }

    IEnumerator HideCoin(float time)
    {
        yield return new WaitForSeconds(time);
        CoinObject.SetActive(false);
    }

    public void ToMainMenu()
    {
        Time.timeScale = 1;
        GameObject.Find("Level Loader").GetComponent<LevelLoader>().LoadScene("MainMenuScene");
    }

    public void LostBall()
    {
        if (Balls.Count == 0)
        {
            Lose();
        }

        shakeTimeElapsed = 0.5f;
    }

    public void Lose()
    {
        if (lifes > 0)
        {
            var newBall = Instantiate(BallPrefab, PositionToLaunchBall.position, Quaternion.identity);
            newBall.GetComponent<Ball>().ScoreModel = this;
            newBall.GetComponent<Ball>().LaunchBall();
            Balls.Add(newBall.GetInstanceID());
            lifes--;
            UpdateHearts();
        }
        else
        {
            IsLose = true;
            OnLose?.Invoke();
            PauseTitleText.text = "You lose";
            foreach (var ball in Balls)
            {
                Destroy(GameObject.Find(ball.ToString()).gameObject);
            }

            ShowPausePanel();
            PauseButtons.SetActive(true);
            PauseButton.SetActive(false);
            PlayButton.SetActive(false);
            SpikeBonusButton.SetActive(false);
            ScoreText.text = "";
            ComboText.text = "";
            PauseButtons.GetComponent<RectTransform>().localPosition = new Vector3(-128, 0, 0);

            int newMaxScore = score;
            if (score > Save.MaxScore)
            {
                NewScoreText.text = $"New score: {score}!";
                newMaxScore = score;
            }
            else
            {
                NewScoreText.text = $"Score: {score}";
                MaxScoreText.text = $"Record: {Save.MaxScore}";
                newMaxScore = Save.MaxScore;
            }

            int newMaxCombo = maxCombo;
            if (maxCombo > Save.MaxCombo)
            {
                NewComboText.text = $"New max combo: {maxCombo}!";
                newMaxCombo = maxCombo;
            }
            else
            {
                NewComboText.text = $"Max combo: {maxCombo}";
                MaxComboText.text = $"Record: {Save.MaxCombo}";
                newMaxCombo = Save.MaxCombo;
            }

            Save.MaxScore = newMaxScore;
            Save.MaxCombo = newMaxCombo;

            SaveSystem.SaveData(Save);

            var levelEnd = "level_end";
            FirebaseAnalytics.LogEvent(levelEnd,
                new Parameter("score", score),
                new Parameter("combo", combo));
            Analytics.CustomEvent(levelEnd, new Dictionary<string, object>
            {
                {"score", score},
                {"combo", combo}
            });
        }
    }

    public void Pause()
    {
        if (IsLose) return;

        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            IsPause = true;
            ShowPausePanel();
            PauseButtons.SetActive(true);
            PauseButton.SetActive(false);
            PauseTitleText.text = "Pause";
        }
        else
        {
            Time.timeScale = 1;
            IsPause = false;
            HidePausePanel();
            PauseButtons.SetActive(false);
            PauseButton.SetActive(true);
            PauseTitleText.text = "";
        }
    }

    private void ShowPausePanel()
    {
        PausePanel.GetComponent<Animator>().enabled = true;
        PausePanel.GetComponent<Animator>().Play("PausePanel");
    }

    private void HidePausePanel()
    {
        PausePanel.GetComponent<Animator>().enabled = true;
        PausePanel.GetComponent<Animator>().Play("UnPausePanel");
    }

    private void UpdateHearts()
    {
        foreach (var heart in Hearts)
        {
            heart.SetActive(false);
        }

        for (var i = 0; i < lifes; i++)
        {
            Hearts[i].SetActive(true);
        }

    }
}