﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    private static BackgroundMusic backgroundMusic;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(transform.gameObject);
        PlayMusic();

        if (backgroundMusic == null)
        {
            backgroundMusic = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
 
    public void PlayMusic()
    {
        if (audioSource.isPlaying) return;
        audioSource.Play();
    }
 
    public void StopMusic()
    {
        audioSource.Stop();
    }
}
