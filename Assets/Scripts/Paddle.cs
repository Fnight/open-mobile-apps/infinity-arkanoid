﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class Paddle : MonoBehaviour
{
    public Transform LeftBorder;
    public Transform RightBorder;
    public ScoreModel ScoreModel;
    public GameObject StartBall;
    public GameObject SpikesObject;

    private bool isBallLaunch = false;
    private Rigidbody rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ScoreModel.IsLose || ScoreModel.IsPause) return;
        MoveLine();
        if (ScoreModel.SpikeBonus > 0 && !SpikesObject.activeSelf)
        {
            SpikesObject.SetActive(true);
        } else if (ScoreModel.SpikeBonus <= 0 && SpikesObject.activeSelf)
        {
            SpikesObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Split Bonus"))
        {
            SplitBonus(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Across Bonus"))
        {
            AcrossBonus(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Coin"))
        {
            CollectCoin(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Bomb Bonus"))
        {
            BombBonus(other.gameObject);
        } else if (other.gameObject.CompareTag("Block") && ScoreModel.SpikeBonus > 0)
        {
            other.gameObject.GetComponent<Block>().BreakBlock(false);
        }
    }

    private void CollectCoin(GameObject o)
    {
        ScoreModel.CollectCoin();
        Destroy(o);
    }

    private void AcrossBonus(GameObject o)
    {
        ScoreModel.AddAcrossBonus();
        Destroy(o);
    }

    private void BombBonus(GameObject o)
    {
        foreach (var block in GameObject.FindGameObjectsWithTag("Block"))
        {
            block.GetComponent<Block>().BreakBlock(false);
        }
        Destroy(o);
    }

    private void SplitBonus(GameObject o)
    {
        var count = o.GetComponent<SplitBonus>().SplitCount;
        Int32[] oldBalls = new Int32[ScoreModel.Balls.Count];

        ScoreModel.Balls.CopyTo(oldBalls);

        foreach (var ball in oldBalls)
        {
            var obj = GameObject.Find(ball.ToString());
            if (obj != null)
            {
                obj.GetComponent<Ball>().Split(count);
            }
        }

        Destroy(o);
    }

    private void MoveLine()
    {
        if (Input.touchCount > 0)
        {
            if (!isBallLaunch)
            {
                StartBall.transform.position = new Vector3(transform.position.x, StartBall.transform.position.y,
                    StartBall.transform.position.z);

                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    isBallLaunch = true;
                    StartBall.GetComponent<Ball>().LaunchBall();
                }
            }

            MoveLine(Input.GetTouch(0).position.x);
        }

        if (Input.GetMouseButton(0))
        {
            if (!isBallLaunch)
            {
                StartBall.transform.position = new Vector3(transform.position.x, StartBall.transform.position.y,
                    StartBall.transform.position.z);
            }

            MoveLine(Input.mousePosition.x);
        }

        if (Input.GetMouseButtonUp(0) && !isBallLaunch)
        {
            isBallLaunch = true;
            StartBall.GetComponent<Ball>().LaunchBall();
        }
    }

    private void MoveLine(float screenX)
    {
        var width = Screen.width;
        float newX;
        var left = LeftBorder.position.x + transform.localScale.x;
        var right = RightBorder.position.x - transform.localScale.x;
        var size = (float) Math.Sqrt(Math.Pow(right - left, 2)) * (screenX / width);
        if (LeftBorder.position.x > 0)
        {
            newX = left - size;
        }
        else
        {
            newX = left + size;
        }

        transform.position = new Vector3(newX, transform.position.y, transform.position.z);
    }
}