﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuModel : MonoBehaviour
{
    public static Color HalfWhiteColor = new Color(1, 1, 1, 0.3f);

    [SerializeField] private PostProcessVolume ProcessVolume;
    [SerializeField] private GameObject SoundButton;
    [SerializeField] private Texture SoundOffIcon;
    [SerializeField] private Texture SoundOnIcon;
    [SerializeField] private GameObject MusicButton;
    [SerializeField] private Texture MusicOffIcon;
    [SerializeField] private Texture MusicOnIcon;
    [SerializeField] private GameObject Ball;
    [SerializeField] private GameObject MainMenuItems;
    [SerializeField] private GameObject ShopItems;
    [SerializeField] private GameObject ShopCamera;
    [SerializeField] private GameObject SettingsItems;
    [SerializeField] private GameObject MainCamera;
    [SerializeField] private Text SlowQualityText;
    [SerializeField] private Text FastQualityText;
    [SerializeField] private Text ShopMoneyText;
    [SerializeField] private Text VersionLabel;
    [SerializeField] private GameObject[] WallToColor;
    [SerializeField] private UnityAdsManager AdsManager;
    [SerializeField] private GameObject LeftShopButton;
    [SerializeField] private GameObject RightShopButton;
    [SerializeField] private GameObject[] Categories;
    [SerializeField] private Text SpikePriceText;
    [SerializeField] private Text SpikeCountText;
    [SerializeField] private Text MessageText;
    [SerializeField] private Text LifesPriceText;
    [SerializeField] private GameObject LifeUpgradeButton;
    [SerializeField] private GameObject[] HeartsLife;
    [SerializeField] private int HeartUpgradePrice = 20;
    [SerializeField] private RawImage[] PagintaionImages;
    [SerializeField] private Texture FillCircleImage;
    [SerializeField] private Texture EmptyCircleImage;

    public int MoneyRewardForAds = 20;
    public int SpikeBonusPrice = 20;
    public Action<string> SkinChanged;

    public SaveDto Save { private set; get; }

    private int currentShopPage = 1;
    private int countShopCategoires = 2;

    public void Start()
    {
        Ball.GetComponent<Ball>().LaunchBall();
        Save = SaveSystem.LoadSave();
        UpdateMoneyText();
        ColorWall();
        UpdateQualityLabels();
        UpdateGraphics();
        UpdateShopCategories();
        UpdateSpikeLabels();
        UpdateHeartsLifes();
        if (!Save.IsSoundOn)
        {
            SoundButton.GetComponent<RawImage>().texture = SoundOffIcon;
        }
        if (!Save.IsMusicOn)
        {
            MusicButton.GetComponent<RawImage>().texture = MusicOffIcon;
            GameObject.FindWithTag("Music").GetComponent<BackgroundMusic>().StopMusic();
        }
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        VersionLabel.text = $"version: {Application.version}";
    }

    public void UpdateMoneyText()
    {
        ShopMoneyText.text = $"{Save.Money}";
    }

    public void StartGame()
    {
        GameObject.Find("Level Loader").GetComponent<LevelLoader>().LoadScene("GameScene");
    }

    public void OpenShop()
    {
        ShopItems.SetActive(true);
        ShopCamera.SetActive(true);
        MainMenuItems.SetActive(false);
        MainCamera.SetActive(false);
        currentShopPage = 1;
        UpdateShopCategories();
    }

    public void OpenSettings()
    {
        SettingsItems.SetActive(true);
        ShopCamera.SetActive(true);
        MainMenuItems.SetActive(false);
        MainCamera.SetActive(false);
    }
    
    public void CloseSettings()
    {
        SettingsItems.SetActive(false);
        ShopCamera.SetActive(false);
        MainMenuItems.SetActive(true);
        MainCamera.SetActive(true);
    }

    private void UpdateShopCategories()
    {
        if (currentShopPage == 1)
        {
            LeftShopButton.SetActive(false);
            RightShopButton.SetActive(true);
        } else if (currentShopPage > 1 && currentShopPage < countShopCategoires)
        {
            LeftShopButton.SetActive(true);
            RightShopButton.SetActive(true);
        }
        else
        {
            LeftShopButton.SetActive(true);
            RightShopButton.SetActive(false);
        }

        foreach (var category in Categories)
        {
            category.SetActive(false);
        }
        Categories[currentShopPage - 1].SetActive(true);

        foreach (var pagination in PagintaionImages)
        {
            pagination.texture = EmptyCircleImage;
        }

        PagintaionImages[currentShopPage - 1].texture = FillCircleImage;
    }

    public void ChangeQuality()
    {
        Save.IsFast = !Save.IsFast;
        UpdateQualityLabels();
        UpdateGraphics();
        SaveSystem.SaveData(Save);
    }

    private void UpdateQualityLabels()
    {
        if (Save.IsFast)
        {
            FastQualityText.color = Color.white;
            SlowQualityText.color = HalfWhiteColor;
        }
        else
        {
            FastQualityText.color = HalfWhiteColor;
            SlowQualityText.color = Color.white;
        }
    }

    private void UpdateGraphics()
    {
        Bloom bloom = null;
        ProcessVolume.profile.TryGetSettings(out bloom);
        if (bloom != null)
        {
            bloom.enabled.value = !Save.IsFast;
        }
    }

    public void CloseShop()
    {
        ShopItems.SetActive(false);
        ShopCamera.SetActive(false);
        MainMenuItems.SetActive(true);
        MainCamera.SetActive(true);
    }

    public void ChangeSound()
    {
        if (Save.IsSoundOn)
        {
            Save.IsSoundOn = false;
            SoundButton.GetComponent<RawImage>().texture = SoundOffIcon;
        }
        else
        {
            Save.IsSoundOn = true;
            SoundButton.GetComponent<RawImage>().texture = SoundOnIcon;
        }
        SaveSystem.SaveData(Save);
    }
    
    public void ChangeMusic()
    {
        var music = GameObject.FindWithTag("Music").GetComponent<BackgroundMusic>();
        if (Save.IsMusicOn)
        {
            music.StopMusic();
            Save.IsMusicOn = false;
            MusicButton.GetComponent<RawImage>().texture = MusicOffIcon;
        }
        else
        {
            music.PlayMusic();
            Save.IsMusicOn = true;
            MusicButton.GetComponent<RawImage>().texture = MusicOnIcon;
        }
        SaveSystem.SaveData(Save);
    }
    
    public void ColorWall()
    {
        foreach (var wall in WallToColor)
        {
            wall.GetComponent<MeshRenderer>().material = Resources.Load<Skin>(Save.CurrentSkin).Block;
        }
    }

    public void GetFreeMoney()
    {
        FirebaseAnalytics.LogEvent("try_free_money");
        AdsManager.ShowRewardedAd(result =>
        {
            if (result != ShowResult.Finished) return;
            Save.Money += MoneyRewardForAds;
            SaveSystem.SaveData(Save);
            FirebaseAnalytics.LogEvent("get_free_money");
            UpdateMoneyText();
        });
    }

    public void GoNextShopCategory()
    {
        currentShopPage++;
        UpdateShopCategories();
    }

    public void GoPrevShopCategory()
    {
        currentShopPage--;
        UpdateShopCategories();
    }

    public void BuySpikeBonus()
    {
        if (Save.Money < SpikeBonusPrice)
        {
            MessageText.text = "No enough money";
            StartCoroutine(ReturnMessageText());
        }
        else
        {
            Save.Money -= SpikeBonusPrice;
            UpdateMoneyText();
            Save.SpikeBonus++;
            SaveSystem.SaveData(Save);
            UpdateSpikeLabels();
            
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEcommercePurchase, 
                new Parameter("type", "bonus"),
                new Parameter("name", "spikes"));
            Analytics.CustomEvent(FirebaseAnalytics.EventEcommercePurchase, new Dictionary<string, object>
            {
                {"type", "bonus"},
                {"name", "spikes"}
            });
        }
    }
    
    IEnumerator ReturnMessageText()
    {
        yield return new WaitForSeconds(1);
        MessageText.text = "";
    }

    private void UpdateSpikeLabels()
    {
        SpikeCountText.text = $"Count: {Save.SpikeBonus}";
        SpikePriceText.text = $"Price: {SpikeBonusPrice}";
    }

    private void UpdateHeartsLifes()
    {
        LifesPriceText.text = $"Price: {HeartUpgradePrice}";
        foreach (var heart in HeartsLife)
        {
            heart.GetComponent<RawImage>().color = HalfWhiteColor;
        }

        for (var i = 0; i < Save.Lifes; i++)
        {
            HeartsLife[i].GetComponent<RawImage>().color = Color.white;
        }

        if (Save.Lifes > 2)
        {
            LifeUpgradeButton.SetActive(false);
        }
    }

    public void UpHearts()
    {
        if (Save.Money < HeartUpgradePrice)
        {
            MessageText.text = "No enough money";
            StartCoroutine(ReturnMessageText());
        }
        else
        {
            Save.Money -= HeartUpgradePrice;
            UpdateMoneyText();
            Save.Lifes++;
            SaveSystem.SaveData(Save);
            UpdateHeartsLifes();
            
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEcommercePurchase, 
                new Parameter("type", "bonus"),
                new Parameter("name", "life"));
            Analytics.CustomEvent(FirebaseAnalytics.EventEcommercePurchase, new Dictionary<string, object>
            {
                {"type", "bonus"},
                {"name", "life"}
            });
        }
    }
}
