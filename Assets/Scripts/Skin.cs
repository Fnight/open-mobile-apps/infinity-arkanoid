using UnityEngine;

[CreateAssetMenu(fileName = "skin", menuName = "Skin")]
public class Skin: ScriptableObject
{
    public Material Block;
    public Material Paddle;
    public int Price;
}