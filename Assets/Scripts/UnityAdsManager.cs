﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsManager : MonoBehaviour
{
    public string GameId = "";
    public bool IsTestMode = false;
    public string RewardedVideoId;

    private void Awake()
    {
        Advertisement.Initialize(GameId, IsTestMode);
    }

    public void ShowRewardedAd(Action<ShowResult> callback)
    {
        if (!Advertisement.IsReady(RewardedVideoId)) return;
        ShowOptions options = new ShowOptions();
        options.resultCallback = callback;
        Advertisement.Show(RewardedVideoId, options);
    }
}
