﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystem
{
    private static string savePath = Application.persistentDataPath + "/save";
    
    public static void SaveData(SaveDto saveDto)
    {
        var formatter = new BinaryFormatter();
        var stream = new FileStream(savePath, FileMode.Create);
        formatter.Serialize(stream, saveDto);
        stream.Close();
    }

    public static SaveDto LoadSave()
    {
        if (File.Exists(savePath))
        {
            var formatter = new BinaryFormatter();
            var stream = new FileStream(savePath, FileMode.Open);

            var data = formatter.Deserialize(stream) as SaveDto;
            stream.Close();

            if (data != null)
            {
                if (data.CurrentSkin == null)
                {
                    data.CurrentSkin = "default";
                }

                if (data.Skins == null)
                {
                    data.Skins = new List<string> {"default"};
                }

                SaveData(data);
            }

            return data;
        }

        var emptySaveDto = new SaveDto
        {
            MaxScore = 0, 
            MaxCombo = 0, 
            IsSoundOn = true,
            IsMusicOn = true,
            Money = 0, 
            CurrentSkin = "default",
            Skins = new List<string>{ "default" },
            IsFast = false,
            SpikeBonus = 0
        };
        return emptySaveDto;
    }
}