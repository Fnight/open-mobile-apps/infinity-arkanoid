﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public float Delay = 2;
    public GameObject Block;
    public ScoreModel ScoreModel;

    private float timer = 0;
    
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        var realDelay = Delay / (ScoreModel.SpeedBonus > 0 ? ScoreModel.SpeedBonusCooficient : 1);
        if (timer < realDelay || ScoreModel.IsLose || ScoreModel.IsPause) return;

        var position = transform.position;
        var width = transform.localScale.x - Block.transform.localScale.x;
        var height = transform.localScale.z - Block.transform.localScale.z;
        var newPosition = new Vector3(Random.Range(position.x - width / 2, position.x + width / 2),
            position.y,
            Random.Range(position.z - height / 2, position.z + height / 2));
        var block = Instantiate(Block, newPosition, Quaternion.identity);
        block.GetComponent<Block>().ScoreModel = ScoreModel;
        timer = 0;
    }
}