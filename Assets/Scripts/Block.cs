﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Block : MonoBehaviour
{
    public float Speed = 1;
    public ScoreModel ScoreModel;
    public ParticleSystem ParticleSystem;
    public GameObject BlockGameObject;
    public BoxCollider Collider;
    public GameObject SplitBonus;
    public GameObject AcrossBonus;
    public GameObject BombBonus;
    public GameObject Coin;

    private void Start()
    {
        if (ScoreModel.AcrossBonus > 0)
        {
            Collider.isTrigger = true;
        }

        GetComponentInChildren<MeshRenderer>().material = ScoreModel.CurrentSkin.Block;
        ParticleSystem.GetComponent<Renderer>().material = ScoreModel.CurrentSkin.Block;
    }

    // Update is called once per frame
    void Update()
    {
        if (ScoreModel.IsLose || ScoreModel.IsPause) return;
        transform.position += Vector3.back * (Speed * (ScoreModel.SpeedBonus > 0 ? ScoreModel.SpeedBonusCooficient : 1));
        ChangeAcrossBonus();
    }

    private void ChangeAcrossBonus()
    {
        Collider.isTrigger = ScoreModel.AcrossBonus > 0;
    }

    public void BreakBlock(bool isCreateDrop)
    {
        Destroy(BlockGameObject);
        ScoreModel.BreakBlock();
        ParticleSystem.Play();
        Collider.enabled = false;
        if (isCreateDrop)
        {
            CreateDrop();
        }

        StartCoroutine(DestroyMe(ParticleSystem.main.startLifetime.constant));
    }

    private void CreateDrop()
    {
        var r = Random.Range(0, 100);
        var position = transform.position;
        var bonusPosition = new Vector3(position.x, position.y, position.z);
        if (r > 95)
        {
            var bonus = Instantiate(SplitBonus, bonusPosition, Quaternion.identity);
            bonus.GetComponent<SplitBonus>().SplitCount = 2;
            bonus.GetComponent<SplitBonus>().ScoreModel = ScoreModel;
        } else if (r > 90)
        {
            var bonus = Instantiate(AcrossBonus, bonusPosition, Quaternion.identity);
            bonus.GetComponent<AcrossBonus>().ScoreModel = ScoreModel;
        } else if (r > 80)
        {
            var bonus = Instantiate(Coin, bonusPosition, Quaternion.identity);
            bonus.GetComponent<Coin>().ScoreModel = ScoreModel;
        } else if (r > 75)
        {
            var bonus = Instantiate(BombBonus, bonusPosition, Quaternion.identity);
            bonus.GetComponent<BombBonus>().ScoreModel = ScoreModel;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dead"))
        {
            ScoreModel.Lose();
            BreakBlock(false);
        }
    }

    IEnumerator DestroyMe(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
