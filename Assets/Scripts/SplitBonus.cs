﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitBonus : MonoBehaviour
{
    public float Speed = 1;
    public ScoreModel ScoreModel;
    public int SplitCount = 2;
    
    // Update is called once per frame
    void Update()
    {
        if (ScoreModel.IsLose || ScoreModel.IsPause) return;
        transform.position += Vector3.back * Speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Dead"))
        {
            Destroy(gameObject);
        }
    }
}
