﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    public ScoreModel ScoreModel;
    public GameObject ParticleObject;

    public AudioClip CollideWithPaddleSound;
    public AudioClip CollideWithBlockSound;
    public AudioClip CollideWithWallSound;
    public float StartSpeedBall = 3;
    public float MaxSpeed = 100f;
    
    private Rigidbody rigidbody;
    private AudioSource audioSource;
    private Vector3 lastFrameVelocity;

    private bool isBallLaunch = false;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        gameObject.name = gameObject.GetInstanceID().ToString();
        if (ScoreModel == null) return;
        ScoreModel.Balls.Add(gameObject.GetInstanceID());
    }

    private void Update()
    {
        ChangeColorBall();
    }

    public void LaunchBall()
    {
        float x = StartSpeedBall * 2 * Random.value - StartSpeedBall;
        float z = StartSpeedBall * 2 - x;
        rigidbody.velocity = new Vector3(x, 0, z);
        isBallLaunch = true;
    }

    private void ChangeColorBall()
    {
        if (ScoreModel == null) return;
        if (ScoreModel.AcrossBonus > 0)
        {
            GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
            GetComponent<TrailRenderer>().material.SetColor("_EmissionColor", Color.red);
        }
        else
        {
            GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
            GetComponent<TrailRenderer>().material.SetColor("_EmissionColor", Color.white);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dead"))
        {
            rigidbody.velocity = Vector3.zero;
            ScoreModel.Balls.RemoveAll(id => id == gameObject.GetInstanceID());
            ScoreModel.LostBall();
            Destroy(gameObject);
        } else if (other.CompareTag("Block"))
        {
            DestroyBlock(other.gameObject);
            
            if (ScoreModel.Save.IsSoundOn)
            {
                audioSource.Play();
            }
        }
    }

    private void FixedUpdate()
    {
        if(rigidbody.velocity.magnitude > MaxSpeed)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * MaxSpeed;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.CompareTag("Paddle"))
        {
            ScoreModel.CollidePaddle();
            audioSource.clip = CollideWithPaddleSound;
        } else if (other.gameObject.CompareTag("Block") && isBallLaunch)
        {
            DestroyBlock(other.gameObject);
        }
        else
        {
            audioSource.clip = CollideWithWallSound;
            CreateParticle();
        }

        if (ScoreModel != null && ScoreModel.Save.IsSoundOn)
        {
            audioSource.Play();    
        }
    }

    private void DestroyBlock(GameObject o)
    {
        o.GetComponent<Block>().BreakBlock(true);
        audioSource.clip = CollideWithBlockSound;
        CreateParticle();
    }

    private void CreateParticle()
    {
        var particle = Instantiate(ParticleObject, transform.position, Quaternion.identity);
        StartCoroutine(DestroyMe(particle.GetComponent<ParticleSystem>().main.startLifetime.constant, particle));
    }
    
    IEnumerator DestroyMe(float time, GameObject o)
    {
        yield return new WaitForSeconds(time);
        Destroy(o);
    }

    public void Split(int count)
    {
        var newBall = Instantiate(gameObject, transform.position, Quaternion.identity);
        newBall.GetComponent<Ball>().ScoreModel = ScoreModel;
        newBall.GetComponent<Ball>().LaunchBall();
        ScoreModel.Balls.Add(newBall.GetInstanceID());
    }
}
