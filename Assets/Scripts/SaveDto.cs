﻿using System;
using System.Collections.Generic;

[Serializable]
public class SaveDto
{
    public int MaxScore { get; set; }
    public int MaxCombo { get; set; }
    public bool IsSoundOn { get; set; }
    public bool IsMusicOn { get; set; }
    public int Money { get; set; }
    public string CurrentSkin { get; set; }
    public List<string> Skins { get; set; }
    public bool IsFast { get; set; }
    public int SpikeBonus { get; set; }
    public int Lifes { get; set; }
}

