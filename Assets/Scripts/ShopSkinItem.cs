﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class ShopSkinItem : MonoBehaviour
{
    public GameObject PriceText;
    public GameObject ButtonObject;
    public Text ButtonText;
    public GameObject Paddle;
    public MainMenuModel MenuModel;
    public Text MessageText;
    
    public Skin Skin;

    private bool isSold = false;
    private Button button;
    
    void Start()
    {
        button = ButtonObject.GetComponent<Button>();
        MenuModel.SkinChanged += OnChangeSkin;
        foreach (var child in Paddle.GetComponentsInChildren<MeshRenderer>())
        {
            child.material = Skin.Paddle;
        }
        UpdateItem();
    }

    private void UpdateItem()
    {
        if (MenuModel.Save.Skins.Contains(Skin.name))
        {
            isSold = true;
            PriceText.SetActive(false);
            if (MenuModel.Save.CurrentSkin == Skin.name)
            {
                ButtonText.text = "Current";
                button.onClick.RemoveAllListeners();
            }
            else
            {
                ButtonText.text = "Select";
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(SelectSkin);
            }
        }
        else
        {
            ButtonText.text = "Buy";
            PriceText.GetComponent<Text>().text = Skin.Price.ToString();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(BuySkin);
        }
    }

    private void SelectSkin()
    {
        MenuModel.Save.CurrentSkin = Skin.name;
        SaveSystem.SaveData(MenuModel.Save);
        UpdateItem();
        MenuModel.ColorWall();
        MenuModel.SkinChanged?.Invoke(Skin.name);
    }

    private void BuySkin()
    {
        if (MenuModel.Save.Money < Skin.Price)
        {
            MessageText.text = "No enough money";
            StartCoroutine(ReturnMessageText());
        }
        else
        {
            MenuModel.Save.Money -= Skin.Price;
            MenuModel.UpdateMoneyText();
            MenuModel.Save.Skins.Add(Skin.name);
            MenuModel.Save.CurrentSkin = Skin.name;
            SaveSystem.SaveData(MenuModel.Save);
            MenuModel.SkinChanged?.Invoke(Skin.name);
            MenuModel.ColorWall();
            UpdateItem();
            
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEcommercePurchase, 
                new Parameter("type", "skin"),
                new Parameter("name", Skin.name));
            Analytics.CustomEvent(FirebaseAnalytics.EventEcommercePurchase, new Dictionary<string, object>
            {
                {"type", "skin"},
                {"name", Skin.name}
            });
        }
    }

    private void OnChangeSkin(string name)
    {
        UpdateItem();
    }

    IEnumerator ReturnMessageText()
    {
        yield return new WaitForSeconds(1);
        MessageText.text = "";
    }
}
