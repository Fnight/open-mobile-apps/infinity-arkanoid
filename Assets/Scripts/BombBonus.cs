﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBonus : MonoBehaviour
{
    public float Speed = 1;
    public ScoreModel ScoreModel;

    // Update is called once per frame
    void Update()
    {
        if (ScoreModel.IsLose || ScoreModel.IsPause) return;
        transform.position += Vector3.back * Speed;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Dead"))
        {
            Destroy(gameObject);
        }
    }
}
